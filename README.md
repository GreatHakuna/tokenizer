# Tokenizer

## Palabras reservadas
1. *clase*
1. *si*
1. *ademas*
1. *para*
1. *mientras*
1. *nulo*
1. *imprimir*
1. *retornar*
1. *verdadero*
1. *falso*
1. *var*
1. *fun*
1. *este*
1. *y*
1. *o*
1. *super*

## Uso
### Para promt:
`cargo run`
### Para leer un archivo
`cargo run -- file.foo`
