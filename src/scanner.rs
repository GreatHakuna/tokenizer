use std::collections::HashMap;
use std::fmt;

#[derive(Clone)]
enum TokenType {
    // Reserved Words
    TkClass,
    TkIf,
    TkAnd,
    TkOr,
    TkElse,
    TkFor,
    TkWhile,
    TkNull,
    TkPrint,
    TkReturn,
    TkTrue,
    TkFalse,
    TkVar,
    TkFunction,
    TkThis,
    TkSuper,

    // Simple Symbols
    TkOpenParentheses,
    TkCloseParentheses,
    TkOpenBraces,
    TkCloseBraces,
    TkComma,
    TkDot,
    TkDotComma,
    TkMinus,
    TkPlus,
    TkTimes,
    TkDiv,

    // Complex Symbols
    TkNot,
    TkNotEq,
    TkAssign,
    TkEq,
    TkLT,
    TkLE,
    TkGT,
    TkGE,

    // Others
    TkId,
    TkStr,
    TkNum,
    TkEOF
}

impl fmt::Display for TokenType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            TokenType::TkClass => write!(f,"Class"),
            TokenType::TkIf => write!(f,"If"),
            TokenType::TkAnd => write!(f,"And"),
            TokenType::TkOr => write!(f,"Or"),
            TokenType::TkElse => write!(f,"Else"),
            TokenType::TkFor => write!(f,"For"),
            TokenType::TkWhile => write!(f,"While"),
            TokenType::TkNull => write!(f,"Null"),
            TokenType::TkPrint => write!(f,"Print"),
            TokenType::TkReturn => write!(f,"Return"),
            TokenType::TkTrue => write!(f,"True"),
            TokenType::TkFalse => write!(f,"False"),
            TokenType::TkVar => write!(f,"Var"),
            TokenType::TkFunction => write!(f,"Function"),
            TokenType::TkThis => write!(f,"This"),
            TokenType::TkSuper => write!(f,"Super"),
            TokenType::TkOpenParentheses => write!(f,"OpenParentheses"),
            TokenType::TkCloseParentheses => write!(f,"CloseParentheses"),
            TokenType::TkOpenBraces => write!(f,"OpenBraces"),
            TokenType::TkCloseBraces => write!(f,"CloseBraces"),
            TokenType::TkComma => write!(f,"Comma"),
            TokenType::TkDot => write!(f,"Dot"),
            TokenType::TkDotComma => write!(f,"DotComma"),
            TokenType::TkMinus => write!(f,"Minus"),
            TokenType::TkPlus => write!(f,"Plus"),
            TokenType::TkTimes => write!(f,"Times"),
            TokenType::TkDiv => write!(f,"Div"),
            TokenType::TkNot => write!(f,"Not"),
            TokenType::TkNotEq => write!(f,"NotEq"),
            TokenType::TkAssign => write!(f,"Assign"),
            TokenType::TkEq => write!(f,"Eq"),
            TokenType::TkLT => write!(f,"LT"),
            TokenType::TkLE => write!(f,"LE"),
            TokenType::TkGT => write!(f,"GT"),
            TokenType::TkGE => write!(f,"GE"),
            TokenType::TkId => write!(f,"Id"),
            TokenType::TkStr => write!(f,"Str"),
            TokenType::TkNum => write!(f,"Num"),
            TokenType::TkEOF => write!(f,"EOF")
        }
    }
}

enum States {
    // Main
    StMain,

    // Divition or comment
    StDiv,
    StDivLineComment,
    StDivBlockCommentA,
    StDivBlockCommentB,

    // String
    StStr,

    // Identifier
    StId,

    // Simple Symbols
    StSSym,

    // Complex Symbols
    StCSym,
    StCSymNot,
    StCSymAssign,
    StCSymLT,
    StCSymGT,

    // Numbers
    StNum,
    StNumFractA,
    StNumFractB,
    StNumExp,
    StNumExpSigA,
    StNumExpSigB,
    StNumExpSolo,
}

#[derive(Clone)]
struct Location {
    line:   u16,
    offset: u16
}

impl Default for Location {
    fn default() -> Self{
        Self {line: 1, offset: 0}
    }
}

impl fmt::Display for Location {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Line {} Offset {}", self.line, self.offset)
    }
}

#[derive(Clone)]
pub struct Token {
    token_type: TokenType,
    lexeme:     String,
    //    literal:   Option< > //TODO Union Float Integer?
    position:   Location
}

impl fmt::Display for Token {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Token: < {} >\nType {} at {}", self.lexeme, self.token_type, self.position)
    }
}

pub struct Scanner {
    source:             String,
    tokens:             Vec<Token>,
    reserved_words:     HashMap<String, TokenType>,
    current_position:   Location
}

impl Scanner {
    pub fn new(mut source: String) -> Self {
        source.push_str(" \0"); //EOF
        Self{
            source,
            tokens: Vec::new(),
            reserved_words: HashMap::from([
                ("clase".to_string(),   TokenType::TkClass),
                ("si".to_string(),      TokenType::TkIf),
                ("ademas".to_string(),    TokenType::TkElse),
                ("para".to_string(),     TokenType::TkFor),
                ("mientras".to_string(),   TokenType::TkWhile),
                ("nulo".to_string(),    TokenType::TkNull),
                ("imprimir".to_string(),   TokenType::TkPrint),
                ("retornar".to_string(),  TokenType::TkReturn),
                ("verdadero".to_string(),    TokenType::TkTrue),
                ("falso".to_string(),   TokenType::TkFalse),
                ("var".to_string(),     TokenType::TkVar),
                ("fun".to_string(),TokenType::TkFunction),
                ("este".to_string(),    TokenType::TkThis),
                ("y".to_string(),    TokenType::TkAnd),
                ("o".to_string(),    TokenType::TkOr),
                ("super".to_string(),   TokenType::TkSuper)
            ]),
            current_position: Location::default()
        }
    }

    pub fn scan_tokens(mut self) -> Vec<Token> {
        let bytes = self.source.as_bytes();
        let mut state = States::StMain;
        let mut lexeme = String::new();

        let mut i = 0;
        let mut c = char::from(bytes[i]);

        let mut last_position = self.current_position.clone();

        lexeme.push(c);
        while c != '\0' {
            match state {
                States::StMain => {
                    last_position = self.current_position.clone();
                    match c {
                        //Ignore spaces
                        ' ' | '\t' => state = States::StMain,
                        // Number
                        '0' ..= '9' => state = States::StNum,

                        // Identifier
                        'A' ..= 'Z' | 'a' ..= 'z' | '_' => state = States::StId,

                        // Simple Symbols
                        '('|')'|'{'|'}'|','|'.'|';'|'-'|'+'|'*' => {
                            state = States::StSSym;
                            lexeme.clear();
                            i -= 1;
                            self.current_position.offset -= 1;
                        },

                        // Complex Symbols
                        '!'|'='|'<'|'>' => {
                            state = States::StCSym;
                            lexeme.clear();
                            i -= 1;
                            self.current_position.offset -= 1;
                        },

                        // String
                        '"' => {
                            state = States::StStr;
                            lexeme.clear();
                        }

                        // Div or comment
                        '/' => state = States::StDiv,

                        // Something Weird
                        _ => println!("Not a valid character at {}", i)
                    }
                }
                States::StDiv => match c {
                    '/' => state = States::StDivLineComment,
                    '*' => state = States::StDivBlockCommentA,
                    _  => {
                        state = States::StMain;
                        self.tokens.push(
                            Token {
                                token_type: TokenType::TkDiv,
                                lexeme:     lexeme.clone().trim().to_string(),
                                position:   last_position.clone() 
                            }
                        );
                        lexeme.clear();
                        i-=1;
                        self.current_position.offset -= 1;
                    }
                },
                States::StDivLineComment => match c {
                    '\n' | '\r'   => {
                        state = States::StMain;
                        lexeme.clear();
                    },
                    _     => state = States::StDivLineComment
                },
                States::StDivBlockCommentA => match c {
                    '*' => state = States::StDivBlockCommentB,
                    _  => state = States::StDivBlockCommentA
                },
                States::StDivBlockCommentB => match c {
                    '/'    => {
                        state = States::StMain;
                        lexeme.clear();
                    },
                    '*' => state = States::StDivBlockCommentB,
                    _  => state = States::StDivBlockCommentA
                },

                // String
                States::StStr => match c {
                    '"' => {
                        lexeme.pop();
                        state = States::StMain;
                        self.tokens.push(
                            Token {
                                token_type: TokenType::TkStr,
                                lexeme:     lexeme.clone(),
                                position:   last_position.clone()
                            }
                        );
                        lexeme.clear();
                    },
                    _  => state = States::StStr
                },

                // Identifier
                States::StId => match c {
                    'A' ..= 'Z' | 'a' ..= 'z' | '_' | '0' ..= '9' => state = States::StId, // Store Id
                    _  => {
                        lexeme.pop();
                        state = States::StMain;
                        let token_type = self.reserved_words.get(&lexeme.trim().to_string());
                        match token_type {
                            Some (token) => {self.tokens.push(
                                Token {
                                    token_type: token.clone(),
                                    lexeme:     lexeme.clone().trim().to_string(),
                                    position:   last_position.clone()
                                }
                            )},
                            None => {self.tokens.push(
                                Token {
                                    token_type: TokenType::TkId,
                                    lexeme:     lexeme.clone().trim().to_string(),
                                    position:   last_position.clone()
                                }
                            )}
                        }
                        lexeme.clear();
                        i -= 1;
                        self.current_position.offset -= 1;
                    }
                },

                // Simple Symbols
                States::StSSym => match c {
                    '(' => {
                        state = States::StMain;
                        self.tokens.push(
                            Token {
                                token_type: TokenType::TkOpenParentheses,
                                lexeme:     lexeme.clone().trim().to_string(),
                                position:   last_position.clone()
                            }
                        );
                        lexeme.clear();
                    },
                    ')' => {
                        state = States::StMain;
                        self.tokens.push(
                            Token {
                                token_type: TokenType::TkCloseParentheses,
                                lexeme:     lexeme.clone().trim().to_string(),
                                position:   last_position.clone()
                            }
                        );
                        lexeme.clear();
                    }, // Gen Token
                    '{' => {
                        state = States::StMain;
                        self.tokens.push(
                            Token {
                                token_type: TokenType::TkOpenBraces,
                                lexeme:     lexeme.clone().trim().to_string(),
                                position:   last_position.clone()
                            }
                        );
                        lexeme.clear();
                    }, // Gen Token
                    '}' => {
                        state = States::StMain;
                        self.tokens.push(
                            Token {
                                token_type: TokenType::TkCloseBraces,
                                lexeme:     lexeme.clone().trim().to_string(),
                                position:   last_position.clone()
                            }
                        );
                        lexeme.clear();
                    }, // Gen Token
                    ',' => {
                        state = States::StMain;
                        self.tokens.push(
                            Token {
                                token_type: TokenType::TkComma,
                                lexeme:     lexeme.clone().trim().to_string(),
                                position:   last_position.clone()
                            }
                        );
                        lexeme.clear();
                    }, // Gen Token
                    '.' => {
                        state = States::StMain;
                        self.tokens.push(
                            Token {
                                token_type: TokenType::TkDot,
                                lexeme:     lexeme.clone().trim().to_string(),
                                position:   last_position.clone()
                            }
                        );
                        lexeme.clear();
                    }, // Gen Token
                    ';' => {
                        state = States::StMain;
                        self.tokens.push(
                            Token {
                                token_type: TokenType::TkDotComma,
                                lexeme:     lexeme.clone().trim().to_string(),
                                position:   last_position.clone()
                            }
                        );
                        lexeme.clear();
                    }, // Gen Token
                    '-' => {
                        state = States::StMain;
                        self.tokens.push(
                            Token {
                                token_type: TokenType::TkMinus,
                                lexeme:     lexeme.clone().trim().to_string(),
                                position:   last_position.clone()
                            }
                        );
                        lexeme.clear();
                    }, // Gen Token
                    '+' => {
                        state = States::StMain;
                        self.tokens.push(
                            Token {
                                token_type: TokenType::TkPlus,
                                lexeme:     lexeme.clone().trim().to_string(),
                                position:   last_position.clone()
                            }
                        );
                        lexeme.clear();
                    }, // Gen Token
                    '*' => {
                        state = States::StMain;
                        self.tokens.push(
                            Token {
                                token_type: TokenType::TkTimes,
                                lexeme:     lexeme.clone().trim().to_string(),
                                position:   last_position.clone()
                            }
                        );
                        lexeme.clear();
                    }, // Gen Token
                    _  => println!("Something Went Wrong at {}", i)

                },

                // Complex Symbols
                States::StCSym => match c {
                    '!' => state = States::StCSymNot,
                    '=' => state = States::StCSymAssign,
                    '<' => state = States::StCSymLT,
                    '>' => state = States::StCSymGT,
                    _  => println!("Something Went Wrong at {}", i)
                },
                States::StCSymNot => match c {
                    '=' => {
                        state = States::StMain;
                        self.tokens.push(
                            Token {
                                token_type: TokenType::TkNotEq,
                                lexeme:     lexeme.clone().trim().to_string(),
                                position:   last_position.clone()
                            }
                        );
                        lexeme.clear();
                    },
                    _  => {
                        state = States::StMain;
                        self.tokens.push(
                            Token {
                                token_type: TokenType::TkNot,
                                lexeme:     lexeme.clone().trim().to_string(),
                                position:   last_position.clone()
                            }
                        );
                        lexeme.clear();
                        i -= 1;
                        self.current_position.offset -= 1;
                    }
                },
                States::StCSymAssign => match c {
                    '=' => {
                        state = States::StMain;
                        self.tokens.push(
                            Token {
                                token_type: TokenType::TkEq,
                                lexeme:     lexeme.clone().trim().to_string(),
                                position:   last_position.clone()
                            }
                        );
                        lexeme.clear();
                    },
                    _  => {
                        state = States::StMain;
                        lexeme.pop();
                        self.tokens.push(
                            Token {
                                token_type: TokenType::TkAssign,
                                lexeme:     lexeme.clone().trim().to_string(),
                                position:   last_position.clone()
                            }
                        );
                        lexeme.clear();
                        i -= 1;
                        self.current_position.offset -= 1;
                    }
                },
                States::StCSymLT => match c {
                    '=' => {
                        state = States::StMain;
                        self.tokens.push(
                            Token {
                                token_type: TokenType::TkLE,
                                lexeme:     lexeme.clone().trim().to_string(),
                                position:   last_position.clone()
                            }
                        );
                        lexeme.clear();
                    },
                    _  => {
                        state = States::StMain;
                        self.tokens.push(
                            Token {
                                token_type: TokenType::TkLT,
                                lexeme:     lexeme.clone().trim().to_string(),
                                position:   last_position.clone()
                            }
                        );
                        lexeme.clear();
                        i -= 1;
                        self.current_position.offset -= 1;
                    }
                },
                States::StCSymGT => match c {
                    '=' => {
                        state = States::StMain;
                        self.tokens.push(
                            Token {
                                token_type: TokenType::TkGE,
                                lexeme:     lexeme.clone().trim().to_string(),
                                position:   last_position.clone()
                            }
                        );
                        lexeme.clear();
                    },
                    _  => {
                        state = States::StMain;
                        self.tokens.push(
                            Token {
                                token_type: TokenType::TkGT,
                                lexeme:     lexeme.clone().trim().to_string(),
                                position:   last_position.clone()
                            }
                        );
                        lexeme.clear();
                        i -= 1;
                        self.current_position.offset -= 1;
                    }
                },

                // Numbers
                States::StNum => match c{
                    '0' ..= '9' => state = States::StNum,
                    'E' | 'e'   => state = States::StNumExp,
                    '.' => state = States::StNumFractA,
                    _  => {
                        state = States::StMain;
                        self.tokens.push(
                            Token {
                                token_type: TokenType::TkNum,
                                lexeme:     lexeme.clone().trim().to_string(),
                                position:   last_position.clone()
                            }
                        );
                        lexeme.clear();
                        i -= 1;
                        self.current_position.offset -= 1;
                    }
                },
                States::StNumFractA => match c {
                    '0' ..= '9' => state = States::StNumFractB,
                    _  => println!("NaN Error at {}", i)
                },
                States::StNumFractB => match c {
                    '0' ..= '9' => state = States::StNumFractB,
                    'E' | 'e'   => state = States::StNumExp,
                    _  => {
                        state = States::StMain;
                        self.tokens.push(
                            Token {
                                token_type: TokenType::TkNum,
                                lexeme:     lexeme.clone().trim().to_string(),
                                position:   last_position.clone()
                            }
                        );
                        lexeme.clear();
                        i -= 1;
                        self.current_position.offset -= 1;
                    }
                },
                States::StNumExp => match c {
                    '+' | '-' => state = States::StNumExpSigA,
                    '0' ..= '9' => state = States::StNumExpSolo,
                    _  => println!("NaN Error at {}", i)
                },
                States::StNumExpSigA => match c {
                    '0' ..= '9' => state = States::StNumExpSigB,
                    _  => println!("NaN Error at {}", i)
                },
                States::StNumExpSigB => match c {
                    '0' ..= '9' => state = States::StNumExpSigB,
                    _  => {
                        state = States::StMain;
                        self.tokens.push(
                            Token {
                                token_type: TokenType::TkNum,
                                lexeme:     lexeme.clone().trim().to_string(),
                                position:   last_position.clone()
                            }
                        );
                        lexeme.clear();
                        i -= 1;
                        self.current_position.offset -= 1;
                    }
                },
                States::StNumExpSolo => match c {
                    '0' ..= '9' => state = States::StNumExpSolo,
                    _  => {
                        state = States::StMain;
                        self.tokens.push(
                            Token {
                                token_type: TokenType::TkNum,
                                lexeme:     lexeme.clone().trim().to_string(),
                                position:   last_position.clone()
                            }
                        );
                        lexeme.clear();
                        i -= 1;
                        self.current_position.offset -= 1;
                    }
                },
            }
            if c != '\n' && c != '\r'{
                self.current_position.offset += 1;
            } else {
                self.current_position.line += 1;
                self.current_position.offset = 0;
            }
            i += 1;
            c = char::from(bytes[i]);
            lexeme.push(c);
        }
        //Token EOF
        self.tokens.push(
            Token {
                token_type: TokenType::TkEOF,
                lexeme:     String::from("EOF"),
                position:   self.current_position
            }
        );
        self.tokens
    }
}
