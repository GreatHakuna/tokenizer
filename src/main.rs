use std::env;
use std::fs::File;
use std::io;
use std::io::prelude::*;
mod scanner;

fn main() -> std::io::Result<()>{
    let mut args = env::args();
    let source = &mut String::new();

    match args.nth(1) {
        Some(path) => {
            let file = File::open(path)?;
            let mut buffer = io::BufReader::new(file);
            buffer.read_to_string(source)?;
            let my_scanner = scanner::Scanner::new(source.clone());
            let tokens = my_scanner.scan_tokens();
            for token in tokens {
                println!("{}\n",token);
            }
        },
        None => {
            loop {
                source.clear();
                print!(">>> ");
                io::stdout().flush()?;
                io::stdin().read_line(source)?;
                if source.as_bytes().len() == 0 {break;}
                let my_scanner = scanner::Scanner::new(source.clone());
                let tokens = my_scanner.scan_tokens();
                for token in tokens {
                    println!("{}\n",token);
                }
            }
        }
    };
    Ok(())
}
